
{smcl}
{* 26 Dec 2021}{...}
{hline}
help for {hi:wind_data}
{hline}

{title:Title}

{p 4 4 2}
{bf:wind_data} —— 将从Wind或金融Choice数据库下载的数据转化成长面板数据

{title:Syntax}

{p 4 4 2}
{cmdab:wind_data} 
{cmdab:TimeType}
[{cmdab:Speed}]

{title:Description}

{p 4 4 2}
{cmd:wind_data} 解放您的双手，点燃您的激情，将时间配置在更需要您的地方，
     只需轻轻一点，数据就在您的眼前！

{title:Options}

{p 4 4 2}  
{bf:TimeType}： 需要转化的数据类型，如果是年度数据，则是year或者y。季度数据，则是quarter或者q {break} 
{bf:Speed}：可选项，选择是否进行加速转化，
输入speed或者s代表加速。加速条件为：{bf:同一变量下的数据类型必须相同。}
即同一个变量下的所有时间点的数据类型要么是数值型要么是字符型。

{title:Examples}

{p 4 4 2} *- 转换年度!. {p_end}
{p 4 4 2}{inp:.} {stata `"wind_data year  或者 wind_data y"'}{p_end}

{p 4 4 2} *- 转换季度!. {p_end}
{p 4 4 2}{inp:.} {stata `"wind_data quarter  或者 wind_data q"'}{p_end}

{p 4 4 2} *- 加速模式!. {p_end}
{p 4 4 2}{inp:.} {stata `"wind_data y s ， wind_data q s"'}{p_end}

{title:Author}

{p 4 4 2}
{cmd:Small Emperor,Ma}{break}
毕业大学：国内某知名大学.{break}
E-mail: Stata官方无法获取该作者信息，可以点击该网址,强制获取{break}
{browse "https://www.iqiyi.com/v_1xkc9zi21j4.html?vfm=2008_aldbd":personal.information.checing.edu.cn } {break}

